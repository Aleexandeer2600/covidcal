package com.example.practicacalccov;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button calc, reset;
    RadioButton op1R, op2R, c5, c6, c7, c8;
    Spinner comboValor;
    TextView spinValor, sexoValor, puntosValor, riesgo;
    String valorRad, valorCheck;
    CheckBox c1, c2, c3, c4;
    RadioGroup caja;
    int puntos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        calc = (Button) findViewById(R.id.btnCal);
        reset = (Button) findViewById(R.id.btnRes);
        op1R = (RadioButton) findViewById(R.id.rB1);
        op2R = (RadioButton) findViewById(R.id.rB2);
        c1 = (CheckBox) findViewById(R.id.check1);
        c2 = (CheckBox) findViewById(R.id.check2);
        c3 = (CheckBox) findViewById(R.id.check3);
        c4 = (CheckBox) findViewById(R.id.check4);
        c5 = (RadioButton) findViewById(R.id.rB3);
        c6 = (RadioButton) findViewById(R.id.rB4);
        c7 = (RadioButton) findViewById(R.id.rB5);
        c8 = (RadioButton) findViewById(R.id.rB6);
        spinValor = (TextView) findViewById((R.id.txtEdad));
        sexoValor = (TextView) findViewById (R.id.txtSexo);
        puntosValor = (TextView) findViewById(R.id.txtPuntos);
        riesgo = (TextView) findViewById(R.id.txtRiesgo);
        comboValor = (Spinner) findViewById(R.id.spinner);
        caja = (RadioGroup) findViewById(R.id.radioGroup2);

        ArrayAdapter<CharSequence> adapter=ArrayAdapter.createFromResource(
                this, R.array.combo_edades, android.R.layout.simple_spinner_item);
        comboValor.setAdapter(adapter);

        op1R.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                valorRad = op1R.getText().toString();
                sexoValor.setText("Usted selecciono: "+ valorRad);
                puntos= puntos + 0;
              //  Toast.makeText(MainActivity.this, "USTED LLEVA PUNTOS: " + puntos, Toast.LENGTH_SHORT).show();
                op2R.setEnabled(false);


            }
        });

        op2R.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                valorRad = op2R.getText().toString();
                sexoValor.setText("Usted selecciono: "+ valorRad);
                puntos= puntos + 1;
             //   Toast.makeText(MainActivity.this, "USTED LLEVA PUNTOS: " + puntos, Toast.LENGTH_SHORT).show();
                op1R.setEnabled(false);

            }
        });

        comboValor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               // Toast.makeText(parent.getContext(), "Seleccionados: "+ parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();

                if(id == 1){
                   // Toast.makeText(parent.getContext(), "Joven, peligro bajo: "+ parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                    puntos= puntos + 0;
                    comboValor.setEnabled(false);
                 //   Toast.makeText(MainActivity.this, "USTED LLEVA PUNTOS: " + puntos, Toast.LENGTH_SHORT).show();
                }
                if(id == 2){
                  //  Toast.makeText(parent.getContext(), "Adulto joven, peligro medio: "+ parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                    puntos= puntos + 1;
                    comboValor.setEnabled(false);
                  //  Toast.makeText(MainActivity.this, "USTED LLEVA PUNTOS: " + puntos, Toast.LENGTH_SHORT).show();
                }
                if(id == 3){
                  //  Toast.makeText(parent.getContext(), "Adulto mayor, peligro alto: "+ parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                    puntos= puntos + 2;
                    comboValor.setEnabled(false);
                  //  Toast.makeText(MainActivity.this, "USTED LLEVA PUNTOS: " + puntos, Toast.LENGTH_SHORT).show();
                }
                if(id == 4){
                  //  Toast.makeText(parent.getContext(), "Person tercera edad, peligro muy alto: "+ parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();
                    puntos= puntos + 3;
                    comboValor.setEnabled(false);
                   // Toast.makeText(MainActivity.this, "USTED LLEVA PUNTOS: " + puntos, Toast.LENGTH_SHORT).show();
                }

                spinValor.setText(" Usted selecciono: "+  parent.getItemAtPosition(position).toString());

            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        /////////////////////////////////////Zona checks
        c1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 1;
                Toast.makeText(MainActivity.this, "Hipertension seleccionada, 1 punto: ", Toast.LENGTH_SHORT).show();
            }
        });
        c2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 1;
                Toast.makeText(MainActivity.this, "Diabetes seleccionada, 1 punto: ", Toast.LENGTH_SHORT).show();
            }
        });
        c3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 2;
                Toast.makeText(MainActivity.this, "Enfermedad pulmonar seleccionada, 2 puntos: ", Toast.LENGTH_SHORT).show();
            }
        });
        c4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 3;
                Toast.makeText(MainActivity.this, "Enfermedad renal seleccionada, 3 puntos: ", Toast.LENGTH_SHORT).show();
            }
        });
        /////////////////////////////////////////////////////////////////////////////////////////////////

        ////////////////////////////////////////Zona groupRadio

        c5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 1;
                Toast.makeText(MainActivity.this, "Bajo peso seleccionada, 1 punto: ", Toast.LENGTH_SHORT).show();
                c6.setEnabled(false);
                c7.setEnabled(false);
                c8.setEnabled(false);

            }
        });
        c6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 0;
                Toast.makeText(MainActivity.this, " Peso normal seleccionada, 0 puntos: ", Toast.LENGTH_SHORT).show();
              //  caja.setEnabled(false);
                c5.setEnabled(false);
                c7.setEnabled(false);
                c8.setEnabled(false);
            }
        });
        c7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 3;
                Toast.makeText(MainActivity.this, "Sobrepeso, 1 punto: ", Toast.LENGTH_SHORT).show();
                caja.setEnabled(false);
                c6.setEnabled(false);
                c5.setEnabled(false);
                c8.setEnabled(false);
            }
        });
        c8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos= puntos + 4;
                Toast.makeText(MainActivity.this, "Obesidad seleccionada, 4 puntos: ", Toast.LENGTH_SHORT).show();
                caja.setEnabled(false);
                c6.setEnabled(false);
                c7.setEnabled(false);
                c5.setEnabled(false);
            }
        });

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////

        calc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             puntosValor.setText("Total de puntos acumulados: " + puntos );

             if(puntos <= 3){
                 riesgo.setText("Nivel de riesgo: bajo");
             }else if (puntos >= 4 || puntos <= 10){
                 riesgo.setText("Nivel de riesgo: intermedio, ¡cuidado!");
             }else if( puntos >= 11){
                 riesgo.setText("Nivel de riesgo: alto, ¡cuidado!, ve al medico");
                }
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                puntos = 0;
                op1R.setEnabled(true);
                op2R.setEnabled(true);

                op1R.setChecked(false);
                op2R.setChecked(false)
                ;
                comboValor.setEnabled(true);
                comboValor.setSelection(0);
                c1.setChecked(false);
                c2.setChecked(false);
                c3.setChecked(false);
                c4.setChecked(false);

                c5.setEnabled(true);
                c6.setEnabled(true);
                c7.setEnabled(true);
                c8.setEnabled(true);

                c5.setChecked(false);
                c6.setChecked(false);
                c7.setChecked(false);
                c8.setChecked(false);
                spinValor.setText("");
                puntosValor.setText("Puntos acumulados:");
                riesgo.setText("Nivel de riesgo:");
                sexoValor.setText("Sexo seleccionado:");

                Toast.makeText(MainActivity.this, "Se reiniciaron los valores", Toast.LENGTH_SHORT).show();

            }
        });
      //  puntosValor.setText(" Puntos acumulados: "+ puntos);
    }
}